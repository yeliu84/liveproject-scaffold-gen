package main

import (
	"flag"
	"fmt"
	"io"
	"os"
)

type FlagValues struct {
	name    string
	path    string
	repoURL string
	isSite  bool
}

var fs *flag.FlagSet
var flagValues = FlagValues{}

func setupFlags(flagValues *FlagValues) *flag.FlagSet {
	fs := flag.NewFlagSet("scaffold-gen", flag.ExitOnError)
	fs.StringVar(&flagValues.name, "n", "", "Project name (required)")
	fs.StringVar(&flagValues.path, "d", "", "Project location on disk (required)")
	fs.StringVar(&flagValues.repoURL, "r", "", "Project remote repository URL (required)")
	fs.BoolVar(&flagValues.isSite, "s", false, "Project will have static assets or not (optional, default: false)")
	return fs
}

func parseFlags(fs *flag.FlagSet, args []string) error {
	return fs.Parse(args)
}

func printEmptyFlagMessage(output io.Writer, flagName string) {
	_, err := fmt.Fprintf(output, "%s cannot be empty\n", flagName)
	if err != nil {
		panic(err)
	}
}

func validateFlags(values *FlagValues, output io.Writer) bool {
	validFlags := true
	if len(values.name) == 0 {
		printEmptyFlagMessage(output, "Project name")
		validFlags = false
	}
	if len(values.path) == 0 {
		printEmptyFlagMessage(output, "Project location")
		validFlags = false
	}
	if len(values.repoURL) == 0 {
		printEmptyFlagMessage(output, "Project repository URL")
		validFlags = false
	}
	return validFlags
}

func generateScaffold(values *FlagValues, output io.Writer) error {
	_, err := fmt.Fprintf(output, "Generating scaffold for project %s in %s\n", values.name, values.path)
	return err
}

func usage() {
	_, err := fmt.Fprintf(fs.Output(), "Usage of %s:\n", fs.Name())
	if err != nil {
		panic(err)
	}
	fs.PrintDefaults()
}

func init() {
	fs = setupFlags(&flagValues)
}

func main() {
	err := parseFlags(fs, os.Args[1:])
	if err != nil {
		_, err := fmt.Fprintf(os.Stderr, "%v\n", err)
		if err != nil {
			panic(err)
		}
		os.Exit(1)
	}
	if !validateFlags(&flagValues, os.Stderr) {
		usage()
		os.Exit(1)
	}

	if err = generateScaffold(&flagValues, os.Stderr); err != nil {
		panic(err)
	}
}
