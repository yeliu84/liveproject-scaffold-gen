package main

import (
	"fmt"
	"io"
	"testing"
)

func TestSetupFlags(t *testing.T) {
	t.Log("Given the need to test setting up the flags in a FlagSet")

	flagValues := FlagValues{}
	fs := setupFlags(&flagValues)

	flagsDefinition := []struct {
		name         string
		defaultValue string
	}{
		{
			name:         "n",
			defaultValue: "",
		},
		{
			name:         "d",
			defaultValue: "",
		},
		{
			name:         "r",
			defaultValue: "",
		},
		{
			name:         "s",
			defaultValue: "false",
		},
	}

	for _, fd := range flagsDefinition {
		t.Logf("\tWhen checking flag %q", fd.name)
		f := fs.Lookup(fd.name)
		if f != nil {
			t.Log("\t\tShould exist ✅")
		} else {
			t.Error("\t\tShould exist ❌")
		}
		if f.DefValue == fd.defaultValue {
			t.Logf("\t\tShould have default value: %q ✅", fd.defaultValue)
		} else {
			t.Errorf("\t\tShould have default value: %q, but got %q ❌", fd.defaultValue, f.DefValue)
		}
	}
}

func TestParseFlags(t *testing.T) {
	t.Log("Given the need to test parse the args")

	argsMap := map[string]struct {
		expectedValue string
		getFlagValue  func(values *FlagValues) string
	}{
		"-n": {"name", func(values *FlagValues) string { return values.name }},
		"-d": {"some_path", func(values *FlagValues) string { return values.path }},
		"-r": {"some_url", func(values *FlagValues) string { return values.repoURL }},
		"-s": {"", func(values *FlagValues) string {
			if values.isSite == false {
				return ""
			}
			return "Some random value"
		}},
	}
	var args []string
	for k, v := range argsMap {
		if v.expectedValue != "" {
			args = append(args, k, v.expectedValue)
		}
	}
	flagValues := FlagValues{}
	fs := setupFlags(&flagValues)

	err := parseFlags(fs, args)
	t.Log("\tWhen checking return value")
	if err == nil {
		t.Log("\t\tShould parse args successfully ✅")
	} else {
		t.Fatal("\t\tShould parse args successfully ❌")
	}

	for k, v := range argsMap {
		t.Logf("\tWhen checking parsed value for flag %s", k)
		flagValue := v.getFlagValue(&flagValues)
		if flagValue == v.expectedValue {
			t.Logf("\t\tShould be expected value: \"%v\" ✅", v.expectedValue)
		} else {
			t.Errorf("\t\tShould be expected value: \"%v\", but got \"%+v\" ❌", v.expectedValue, flagValue)
		}
	}
}

func TestValidateValidFlags(t *testing.T) {
	flagValues := FlagValues{}
	fs := setupFlags(&flagValues)
	err := parseFlags(fs, []string{"-n", "name", "-d", "some_path", "-r", "some_url"})

	t.Log("\tWhen checking result of parsing arguments")
	if err == nil {
		t.Log("\t\tShould parse args successfully ✅")
	} else {
		t.Fatal("\t\tShould parse args successfully ❌")
	}

	t.Log("\tWhen checking the result of validating valid flags")
	if validateFlags(&flagValues, io.Discard) {
		t.Log("\t\tShould pass the validation ✅")
	} else {
		t.Error("\t\tShould pass the validation ❌")
	}
}

type TestWriter struct {
	output []string
}

func (testWriter *TestWriter) Write(p []byte) (n int, err error) {
	testWriter.output = append(testWriter.output, string(p))
	return len(p), nil
}

func TestValidateInvalidFlags(t *testing.T) {
	writer := TestWriter{}
	flagValues := FlagValues{}
	fs := setupFlags(&flagValues)

	err := parseFlags(fs, []string{})
	t.Log("\tWhen checking result of parsing arguments")
	if err == nil {
		t.Log("\t\tShould parse args successfully ✅")
	} else {
		t.Fatal("\t\tShould parse args successfully ❌")
	}

	t.Log("\tWhen checking the result of validating invalid flags")
	if !validateFlags(&flagValues, &writer) {
		t.Log("\t\tShould NOT pass the validation ✅")
	} else {
		t.Fatal("\t\tShould NOT pass the validation ❌")
	}

	errMessages := []string{
		"Project name cannot be empty\n",
		"Project location cannot be empty\n",
		"Project repository URL cannot be empty\n",
	}
	for i, msg := range errMessages {
		if writer.output[i] == msg {
			t.Logf("\t\tShould display error message: %q ✅", msg)
		} else {
			t.Errorf("\t\tShould display error message: %q, but got %q ❌", msg, writer.output[i])
		}
	}
}

func TestGenerateScaffold(t *testing.T) {
	flagValues := FlagValues{
		name: "some name",
		path: "some path",
	}
	writer := TestWriter{}

	err := generateScaffold(&flagValues, &writer)
	t.Log("\tWhen checking result of generating scaffold")
	if err == nil {
		t.Log("\t\tShould succeed ✅")
	} else {
		t.Fatal("\t\tShould succeed ❌")
	}

	t.Log("\tWhen checking the out of generating scaffold")
	expected := fmt.Sprintf("Generating scaffold for project %s in %s\n", flagValues.name, flagValues.path)
	if writer.output[0] == expected {
		t.Logf("\t\tShould output: %q ✅", expected)
	} else {
		t.Errorf("\t\tShould output: %q, but got %q ❌", expected, writer.output[0])
	}
}
