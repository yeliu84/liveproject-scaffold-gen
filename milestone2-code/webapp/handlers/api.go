package handlers

import (
	"fmt"
	"net/http"
)

type apiHandler struct{}

func (apiHandler) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	_, err := fmt.Fprintln(w, "You have reached Echorand Corp’s Service API")
	if err != nil {
		logger.Println(err)
	}
}
