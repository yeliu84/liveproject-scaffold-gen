package handlers

import "net/http"

type indexHandler struct{}

func (indexHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./static/index.html")
}
