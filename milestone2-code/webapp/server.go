package main

import (
	"log"
	"net/http"
	"webapp/handlers"
)

func main() {
	mux := http.NewServeMux()
	handlers.Register(mux)
	server := &http.Server{
		Addr:    ":8080",
		Handler: mux,
	}
	log.Println("starting server at 0.0.0.0:8080")
	log.Fatal(server.ListenAndServe())
}
